(** common helpers for windows *)

(**
   Replace slashes with backslashes and vice versa.
   Also, uppercase the drive letter if present

   they will return the unmodified string on *nix
*)
val slashify : string -> string
val unslashify : string -> string

(* first argument: log function, second: filename

   relative filenames are fully resolved in order to
   circumvent problems with bind mounts.

   the function will return the unmodifeid string
   on *nix
*)
val cygpath : (string -> unit) -> string -> string

(**
  example:
  let prog,args =
    cygwin_shell_rewrite
    ~shell_cmd:"dash.exe"
    ~shell_args:["-ec"]
    ~force_shell:false
    "foo"
    ["-p1";"-p2";"bar"] ;;

  the function will look up foo in path and decides, if foo is a native
  windows executable that can be called natively or if it could be a shell
  script that should be called with cygwin.
  The function will return either ("C:\\path\\foo.exe",["-p1";"-p2";"bar"]) or
  ("dash.exe",["-ec";"foo -p1 -p2 bar"])
  (The command for the unix shell properly quoted)

   On non-windows systems, the function will always return the unmodified
   last two parameters ("foo",["-p1";"-p2";"bar"])
*)
val cygwin_shell_rewrite :
  shell_cmd:string ->
  shell_args:string list ->
  force_shell:bool ->
  string ->
  string list ->
  string * string list

module Win32_only : sig
  (** functions inside this module should never be called on
      non-windows systems *)

  (** regular .exe files are executable, .bat files and shell scripts are not.
      The test is done with windows api.

      Known bug: If you you are using a 32-bit version of windows,
      64-bit dlls are sometimes reported as executables.  *)
  val is_executable : string -> bool

  (** like Unix.wait, but you must supply an array of pids of your childrens *)
  val wait : int array -> int * Unix.process_status

  type sresult =
    | Nothing_found (** nothing found in path **)
    | Maybe_script (** it could be a script **)
    | Exe of string (** native windows executable **)

  (**
     searchs for an executable in path (Sys.getenv "PATH" is used as default.

     It is assumed that the passed string doesn't  contain any references to
     a folder (just "foo", not "foo/bar", "C:/foo/bar" or ".\foo" )
  *)
  val search_in_path: ?paths:string list -> string -> sresult

  (** inherit: bInheritHandles of CreateProcess. It is false by default! *)
  val create_process_full :
    ?env:string array ->
    ?cwd:string ->
    ?inherits:bool ->
    cmd:string ->
    args:string array ->
    stdin:Unix.file_descr ->
    stdout:Unix.file_descr ->
    stderr:Unix.file_descr ->
    unit ->
    int
end

module Unix: sig
  (** The unix version of Filename.quote - for all systems, incl. Windows *)
  val quote : string -> string
end
(*
val path_lookup_win32 : string array -> string ->
  [> `Exe of string | `Maybe_script | `None ]
*)
