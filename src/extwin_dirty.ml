module Sys = struct
  include Sys
  include Extwin_utf8.Sys
end

module Unix = struct
  include Unix
  include Extwin_utf8.Unix
end

module Filename = struct
  include Filename
  include Extwin_utf8.Filename
end

include Extwin_utf8.Pervasives
