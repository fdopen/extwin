(** removes string from environment *)
val unsetenv: string -> unit

val is_wow64: unit -> bool
