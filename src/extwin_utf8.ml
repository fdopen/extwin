external getenv: string -> string = "caml_extwin_getenv"

module Ext_unix = struct
  module U = Unix
  module UB = Uwt_base
  module UF = Uv_fs_sync

  let ufail ?(param="") ecode s1 =
    raise (U.Unix_error(UB.to_unix_error ecode,s1,param))

  let trans_of accu = function
  | U.O_RDONLY -> UB.Fs_types.O_RDONLY :: accu
  | U.O_WRONLY -> UB.Fs_types.O_WRONLY :: accu
  | U.O_RDWR  -> UB.Fs_types.O_RDWR :: accu
  | U.O_NONBLOCK -> UB.Fs_types.O_NONBLOCK :: accu
  | U.O_APPEND -> UB.Fs_types.O_APPEND :: accu
  | U.O_CREAT -> UB.Fs_types.O_CREAT :: accu
  | U.O_TRUNC -> UB.Fs_types.O_TRUNC :: accu
  | U.O_EXCL -> UB.Fs_types.O_EXCL :: accu
  | U.O_NOCTTY -> UB.Fs_types.O_NOCTTY :: accu
  | U.O_DSYNC -> UB.Fs_types.O_DSYNC :: accu
  | U.O_SYNC -> UB.Fs_types.O_SYNC :: accu
  | U.O_RSYNC -> UB.Fs_types.O_RSYNC :: accu
  | U.O_SHARE_DELETE
  | U.O_CLOEXEC -> accu

  let openfile fln flags perm =
    let mode = List.fold_left trans_of [] flags in
    match UF.openfile ~mode ~perm fln with
    | Error x -> ufail ~param:fln x "openfile"
    | Ok file ->
      match UB.Conv.file_descr_of_file file with
      | None ->
        let _ : unit Uwt_base.uv_result = UF.close file in
        ufail ~param:fln UB.UNKNOWN "openfile"
      | Some x ->
        if List.mem U.O_CLOEXEC flags = false then
          Unix.clear_close_on_exec x;
        x

  let uw ?param name x =
    match x with
    | Error e -> ufail ?param e name
    | Ok res -> res

  let unlink param =
    UF.unlink param |> uw ~param "unlink"

  let mkdir param perm =
    UF.mkdir ~perm param |> uw ~param "mkdir"

  let rmdir param =
    UF.rmdir param |> uw ~param "rmdir"

  let file_kind = function
  | UF.S_UNKNOWN (* see stat.c *)
  | UF.S_REG -> U.S_REG
  | UF.S_DIR -> U.S_DIR
  | UF.S_CHR -> U.S_CHR
  | UF.S_BLK -> U.S_BLK
  | UF.S_LNK -> U.S_LNK
  | UF.S_FIFO -> U.S_FIFO
  | UF.S_SOCK -> U.S_SOCK

  let stat_convert x =
    let open Unix in
    {
      st_dev = x.UF.st_dev;
      st_ino = x.UF.st_ino;
      st_kind = file_kind x.UF.st_kind;
      st_perm = x.UF.st_perm;
      st_nlink = x.UF.st_nlink;
      st_uid = x.UF.st_uid;
      st_gid = x.UF.st_gid;
      st_rdev = x.UF.st_rdev;
      st_size = Int64.to_int x.UF.st_size;
      st_atime = Int64.to_float x.UF.st_atime;
      st_mtime = Int64.to_float x.UF.st_mtime;
      st_ctime = Int64.to_float x.UF.st_ctime;
    }

  let stat param =
    UF.stat param |> uw ~param "stat" |> stat_convert

  let lstat param =
    UF.lstat param |> uw ~param "lstat" |> stat_convert

  let rename param dst =
    UF.rename ~src:param ~dst |> uw ~param "rename"

  let link param link_name =
    UF.link ~target:param ~link_name |> uw ~param "link"

  let symlink
#if not (OCAML_MAJOR < 4 || (OCAML_MAJOR = 4 && OCAML_MINOR < 3))
    ?(to_dir=false)
#endif
      param dst =
#if OCAML_MAJOR < 4 || (OCAML_MAJOR = 4 && OCAML_MINOR < 3)
    let to_dir = false in
#endif
    let mode = if to_dir then UF.S_Dir else UF.S_Default in
    UF.symlink ~mode ~src:param ~dst () |> uw ~param "symlink"

  let readlink param =
    UF.readlink param |> uw ~param "readlink"

  let utimes param access modif =
    UF.utime param ~access ~modif |> uw ~param "utime"

  let access_map = function
  | U.R_OK -> UF.Read
  | U.W_OK -> UF.Write
  | U.X_OK -> UF.Exec
  | U.F_OK -> UF.Exists

  let access param pl =
    let pl = List.map access_map pl in
    UF.access param pl |> uw ~param "access"

  let chmod param perm =
    UF.chmod param ~perm |> uw ~param "chmod"

  let fchmod file perm =
    match UB.Conv.file_of_file_descr file with
    | None -> ufail UB.UNKNOWN "fchmod"
    | Some fd ->
      uw "fchmod" (UF.fchmod fd ~perm)

  type e
  external environment_init: unit -> e = "caml_extwin_environment_init"
  external environment_clean: e -> unit = "caml_extwin_environment_clean"
  external environment: e -> string array = "caml_extwin_environment"

  let environment () =
    let e = environment_init () in
    try
      environment e
    with
    | x ->
      environment_clean e;
      raise x

  let getenv = getenv
  external putenv: string -> string option -> unit = "caml_extwin_putenv"
  let putenv a b = putenv a (Some b)

  let create_process = Extwin_utf8_proc.create_process
  let create_process_env = Extwin_utf8_proc.create_process_env

  external getcwd: unit -> string = "caml_extwin_getcwd"
  external chdir: string -> unit = "caml_extwin_chdir"

  let getlogin () =
    try
      getenv "USERNAME"
    with
    | Not_found
    | Unix.Unix_error _ -> ""
end

let uvraise ?fln x =
  let emassage = Uwt_base.strerror x in
  match fln with
  | None -> raise(Sys_error emassage)
  | Some x -> raise (Sys_error (x ^ ": " ^ emassage))

module Ext_sys = struct
  external sys_argv : string array -> string array =
    "caml_extwin_sys_argv_byte" "caml_extwin_sys_argv_native"

  let argv =
    match sys_argv Sys.argv with
    | [| |] -> Sys.argv
    | x -> x

  type exe_name =
    | Native (* use uwt *)
    | Error_x (* ups *)
    | Byte of string (* we convert only Sys.executable_name to UTF8 *)

  external get_exe_name : string -> exe_name =
    "caml_extwin_sys_exe_byte" "caml_extwin_sys_exe_native"

  external get_exe_name_byte : string -> exe_name = "caml_extwin_sys_exe_byte"

  let executable_name =
    match get_exe_name Sys.executable_name with
    | Byte s -> s
    | Error_x -> Sys.executable_name
    | Native ->
      match Uwt_base.Misc.exepath () with
      | Ok x -> x
      | Error _ ->
        match get_exe_name_byte Sys.executable_name with
        | Byte s -> s
        | Error_x | Native -> Sys.executable_name

  let getenv s =
    try
      getenv s
    with
    (* see caml_sys_getenv, only Not_found possible *)
    | Unix.Unix_error _ -> raise Not_found

  module UF = Uv_fs_sync
  let file_exists s =
    match UF.stat s with
    | Ok _ -> true
    | Error _ -> false

  let is_directory s =
    match UF.stat s with
    | Ok x -> x.UF.st_kind = UF.S_DIR
    | Error x -> uvraise ~fln:s x

  let remove s =
    match UF.unlink s with
    | Ok () -> ()
    | Error x -> uvraise ~fln:s x

  let rename src dst =
    match UF.rename ~src ~dst with
    | Ok () -> ()
    | Error x -> uvraise ~fln:src x

  external chdir: string -> unit Uwt_base.Int_result.t = "caml_extwin_sys_chdir"
  let chdir s =
    let t = chdir s in
    if Uwt_base.Int_result.is_ok t then
      ()
    else
      uvraise ~fln:s (Uwt_base.Int_result.to_error t)

  external getcwd: unit -> string Uwt_base.uv_result = "caml_extwin_sys_getcwd"
  let getcwd () =
    match getcwd () with
    | Ok s -> s
    | Error x -> uvraise x

  let readdir s =
    match Uv_fs_sync.scandir s with
    | Error x -> uvraise ~fln:s x
    | Ok ar ->
      Array.fold_right ( fun (_,el) ac ->
          (* don't trust libuv manuals *)
          match el with
          | "." | ".." -> ac
          | _ -> el::ac ) ar []
      |> Array.of_list
end

module Ext_pervasives = struct
  module UF = Uv_fs_sync
  let trans accu = function
  |	Open_rdonly -> UF.O_RDONLY::accu
  |	Open_wronly -> UF.O_WRONLY::accu
  |	Open_append -> UF.O_APPEND::accu
  |	Open_creat -> UF.O_CREAT::accu
  |	Open_trunc -> UF.O_TRUNC::accu
  |	Open_excl -> UF.O_EXCL::accu
  |	Open_nonblock -> UF.O_NONBLOCK::accu
  |	Open_binary
  |	Open_text -> accu

  let open_gen mode perm fln =
    let mode = List.fold_left trans [] mode in
    match UF.openfile ~perm ~mode fln with
    | Error x ->  uvraise ~fln x
    | Ok file ->
      match Uwt_base.Conv.file_descr_of_file file with
      | None ->
        let _ : unit Uwt_base.uv_result = UF.close file in
        raise (Sys_error (fln ^ ": open failed"))
      | Some x -> x

  let eunix fd fln =
    (try Unix.close fd with Unix.Unix_error _ -> ());
    raise (Sys_error (fln ^ ": open failed"))

  let open_out_gen mode perm fln =
    let ufd = open_gen mode perm fln in
    match Unix.out_channel_of_descr ufd with
    | exception (Unix.Unix_error _) -> eunix ufd fln
    | oc ->
      if List.mem Open_text mode then (
        try
          set_binary_mode_out oc false
        with
        | x -> close_out oc ; raise x
      );
      oc

  let open_out name =
    open_out_gen [Open_wronly; Open_creat; Open_trunc; Open_text] 0o666 name

  let open_out_bin name =
    open_out_gen [Open_wronly; Open_creat; Open_trunc; Open_binary] 0o666 name

  let open_in_gen mode perm fln =
    let ufd = open_gen mode perm fln in
    match Unix.in_channel_of_descr ufd with
    | exception(Unix.Unix_error _) -> eunix ufd fln
    | ic ->
      if List.mem Open_text mode then (
        try
          set_binary_mode_in ic false
        with
        | x -> close_in ic ; raise x
      );
      ic

  let open_in name =
    open_in_gen [Open_rdonly; Open_text] 0 name

  let open_in_bin name =
    open_in_gen [Open_rdonly; Open_binary] 0 name
end

module Ext_filename = struct

  let temp_dir_name =
    try Ext_sys.getenv "TEMP" with Not_found -> "."

  let current_temp_dir_name = ref temp_dir_name

  let set_temp_dir_name s =
    current_temp_dir_name := s

  let get_temp_dir_name () =
    !current_temp_dir_name

  let prng = lazy(Random.State.make_self_init ())

  let temp_file_name temp_dir prefix suffix =
    let rnd = (Random.State.bits (Lazy.force prng)) land 0xFFFFFF in
    let n = String.concat "" [prefix; Printf.sprintf "%06x" rnd ; suffix] in
    Filename.concat temp_dir n

  let temp_file ?(temp_dir = !current_temp_dir_name) prefix suffix =
    let module UF = Uv_fs_sync in
    let rec try_name counter =
      let name = temp_file_name temp_dir prefix suffix in
      let mode = [UF.O_WRONLY; UF.O_CREAT; UF.O_EXCL] in
      match UF.openfile ~perm:0o600 ~mode name with
      | Error x ->
        if counter >= 1000 then
          uvraise ~fln:name x
        else
          try_name (counter + 1)
      | Ok file ->
        match UF.close file with
        | Ok () -> name
        | Error x ->
          uvraise ~fln:name x
    in
    try_name 0

  let open_temp_file ?(mode = [Open_text])
#if not (OCAML_MAJOR < 4 || (OCAML_MAJOR = 4 && OCAML_MINOR < 3))
      ?(perms = 0o600)
#endif
      ?(temp_dir = !current_temp_dir_name)
      prefix suffix =
#if OCAML_MAJOR < 4 || (OCAML_MAJOR = 4 && OCAML_MINOR < 3)
    let perms = 0o600 in
#endif
    let rec try_name counter =
      let name = temp_file_name temp_dir prefix suffix in
      try
        (name,
         Ext_pervasives.open_out_gen
           (Open_wronly::Open_creat::Open_excl::mode) perms name)
      with
      | Sys_error _ as e ->
        if counter >= 1000 then raise e else try_name (counter + 1)
    in
    try_name 0
end

module Pervasives = Ext_pervasives
module Sys = Ext_sys
module Filename = Ext_filename
module Unix = Ext_unix
