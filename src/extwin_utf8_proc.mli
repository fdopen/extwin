(** inherits is false by default.
    If the file descriptor is a tty handle, it will be inherited anyway *)
val create_process_full :
  ?env:string array ->
  ?cwd:string ->
  ?inherits:bool ->
  cmd:string ->
  args:string array ->
  stdin:Unix.file_descr ->
  stdout:Unix.file_descr ->
  stderr:Unix.file_descr ->
  unit ->
  int

val create_process :
  string ->
  string array ->
  Unix.file_descr ->
  Unix.file_descr ->
  Unix.file_descr -> int

val create_process_env :
  string ->
  string array ->
  string array ->
  Unix.file_descr -> Unix.file_descr -> Unix.file_descr -> int
