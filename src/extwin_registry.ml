type key =
  | HKLM (*  HKEY_LOCAL_MACHINE *)
  | HKCC (* HKEY_CURRENT_CONFIG *)
  | HKCR (* HKEY_CLASSES_ROOT *)
  | HKU (* HKEY_USERS *)
  | HKCU (* HKEY_CURRENT_USER *)

external get_string :
  key ->
  subkey:string ->
  value:string ->
  string option = "caml_extwin_regquery_string"

let get_string key ~subkey ~value =
  if subkey = "" || value = "" then
    None
  else
    get_string key ~subkey ~value

external get_subkeys :
  key:key ->
  subkey:string ->
  string array option = "caml_extwin_regquery_subkeys"

let get_subkeys key subkey =
  if subkey = "" then
    None
  else
    get_subkeys ~key ~subkey
