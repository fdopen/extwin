type key =
  | HKLM (** HKEY_LOCAL_MACHINE **)
  | HKCC (** HKEY_CURRENT_CONFIG **)
  | HKCR (** HKEY_CLASSES_ROOT **)
  | HKU  (** HKEY_USERS **)
  | HKCU (** HKEY_CURRENT_USER **)

val get_string : key -> subkey:string -> value:string -> string option
val get_subkeys: key -> string -> string array option
