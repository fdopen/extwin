type io_in =
  [ `Fd of Unix.file_descr
  | `Null
  | `String of string ]

type io_out =
  [ `Buffer of Buffer.t
  | `Fd of Unix.file_descr
  | `Fun of string -> unit
  | `Null
  | `Stderr
  | `Stdout ]

module Uo = Unix
module Ue = Extwin_utf8.Unix

type pipe_state =
  | Open
  | Closed
  | Uninit

type pipe_with_status =
  {
    mutable state: pipe_state;
    mutable fd: Uo.file_descr
  }

let null_device = Extwin_internal_helper.null_device
let try_finalize = Extwin_internal_helper.try_finalize

let pipe a b =
  let tmp1,tmp2 = Uo.pipe () in
  a.state <- Open;
  a.fd <- tmp1;
  b.state <- Open;
  b.fd <- tmp2

let new_pipe () =
  {state = Uninit;
   fd = Uo.stderr}

let rec eintr1 f a =
  try
    f a
  with
  | Uo.Unix_error(Uo.EINTR,_,_) ->  eintr1 f a

let rec eintr2 f a b =
  try
    f a b
  with
  | Uo.Unix_error(Uo.EINTR,_,_) -> eintr2 f a b

let rec eintr3 f a b c =
  try
    f a b c
  with
  | Uo.Unix_error(Uo.EINTR,_,_) -> eintr3 f a b c

let rec eintr4 f a b c d=
  try
    f a b c d
  with
  | Uo.Unix_error(Uo.EINTR,_,_) -> eintr4 f a b c d

let rec eintr6 f a b c d e g =
  try
    f a b c d e g
  with
  | Uo.Unix_error(Uo.EINTR,_,_) ->
    eintr6 f a b c d e g

let close_pipe a =
  match a.state with
  | Closed
  | Uninit -> ()
  | Open ->
    a.state <- Closed ;
    eintr1 Uo.close a.fd

let close_pipe_ne a = try close_pipe a with |_ -> ()

let str_buffer_len = 8192 (* 32768 *)

let run ?(env=Ue.environment ()) ?(stdin=`Null) ?(stderr=`Stderr) ?(stdout=`Stdout) prog args : int =
  let tmp_str = Bytes.create str_buffer_len
  and p_stdout_read = new_pipe ()
  and p_stdout_write = new_pipe ()
  and p_stderr_read = new_pipe ()
  and p_stderr_write = new_pipe ()
  and p_stdin_read = new_pipe ()
  and p_stdin_write = new_pipe ()
  and args = Array.of_list (prog::args)
  in
  try_finalize ( fun () ->
      let () =
        let comm p fd =
          let fd = eintr1 Uo.dup fd in
          p.fd <-  fd;
          p.state <- Open
        in

        begin match stdout with
        | `Stdout -> p_stdout_write.fd <- Uo.stdout
        | `Stderr -> p_stdout_write.fd <- Uo.stderr
        | `Null ->
          let fd = eintr3 Ue.openfile null_device [ Uo.O_WRONLY ] 0o600 in
          p_stdout_write.fd <- fd;
          p_stdout_write.state <- Open
        | `Fd fd ->
          p_stdout_write.fd <- fd
        | _ -> pipe p_stdout_read p_stdout_write
        end;

        begin match stderr with
        | `Stdout -> p_stderr_write.fd <- Uo.stdout
        | `Stderr -> p_stderr_write.fd <- Uo.stderr
        | `Null ->
          let fd = eintr3 Ue.openfile null_device [ Uo.O_WRONLY ] 0o600 in
          p_stderr_write.fd <- fd;
          p_stderr_write.state <- Open
        | `Fd fd ->
          p_stderr_write.fd <- fd;
        | _ -> pipe p_stderr_read p_stderr_write;
        end;

        begin match stdin with
        | `Null ->
          let fd = eintr3 Ue.openfile null_device [ Uo.O_RDONLY ] 0o400 in
          p_stdin_read.fd <- fd;
          p_stdin_read.state <- Open
        | `Fd fd ->
          comm p_stdin_read fd;
        | _ -> pipe p_stdin_read  p_stdin_write;
        end;
      in

      if p_stdin_write.state = Open then
        Uo.set_close_on_exec p_stdin_write.fd;

      if p_stdout_read.state = Open then
        Uo.set_close_on_exec p_stdout_read.fd;

      if p_stderr_read.state = Open then
        Uo.set_close_on_exec p_stderr_read.fd;

      let pid =
        if Sys.win32 = false then
          eintr6 Ue.create_process_env prog args env p_stdin_read.fd p_stdout_write.fd p_stderr_write.fd
        else
          Extwin_utf8_proc.create_process_full
            ~inherits:false
            ~cmd:prog
            ~args
            ~env
            ~stdin:p_stdin_read.fd
            ~stdout:p_stdout_write.fd
            ~stderr:p_stderr_write.fd
            ()
      in

      close_pipe p_stdout_write;
      close_pipe p_stdin_read;
      close_pipe p_stderr_write;

      let f_read r =
        let is_stdout =
          if r = p_stderr_read.fd then
            false
          else (
            assert ( r = p_stdout_read.fd );
            true
          )
        in
        let x =
          try eintr4 Uo.read r tmp_str 0 str_buffer_len
          with | Uo.Unix_error _ -> -1
        in
        if x <= 0 then (
          if is_stdout then
            close_pipe p_stdout_read
          else
            close_pipe p_stderr_read
        )
        else
          match if is_stdout then stdout else stderr with
          | `Fd _
          | `Null
          | `Stdout
          | `Stderr -> ()
          | `Buffer b -> Buffer.add_subbytes b tmp_str 0 x
          | `Fun (f: string -> unit) -> f (Bytes.sub_string tmp_str 0 x)
      in
      let to_write = match stdin with
      | `Fd _
      | `String ""
      | `Null -> close_pipe p_stdin_write; ref ""
      | `String str -> ref str
      in
      while p_stdout_read.state = Open || p_stderr_read.state = Open || p_stdin_write.state = Open do
        let wl = if p_stdin_write.state = Open then [p_stdin_write.fd] else [] in
        let rl = if p_stderr_read.state = Open then [p_stderr_read.fd] else [] in
        let rl = if p_stdout_read.state = Open then p_stdout_read.fd :: rl else rl in
        let r,w,_ = eintr4 Uo.select rl wl [] 3. in
        List.iter f_read r ;
        match w with
        | [] -> ()
        | [fd] ->
          assert (p_stdin_write.fd = fd);
          let str_len = String.length !to_write in
          assert (str_len > 0 );
          let n_written = eintr4 Uo.write_substring fd !to_write 0 str_len in
          if n_written >= str_len then (
            to_write := "";
            close_pipe p_stdin_write
          )
          else
            to_write := String.sub !to_write n_written (str_len - n_written)
        | _ -> assert false
      done;
      close_pipe p_stdout_read;
      close_pipe p_stderr_read;

      let _, process_status = eintr2 Uo.waitpid [] pid in
      let ret_code = match process_status with
      | Uo.WEXITED n -> n
      | Uo.WSIGNALED _ -> 2 (* like OCaml's uncaught exceptions *)
      | Uo.WSTOPPED _ ->
        (* only possible if the call was done using WUNTRACED
           or when the child is being traced *)
        3
      in
      ret_code
    ) () ( fun () ->
      close_pipe_ne p_stdin_read;
      close_pipe_ne p_stdin_write;
      close_pipe_ne p_stdout_read;
      close_pipe_ne p_stdout_write;
      close_pipe_ne p_stderr_write;
      close_pipe_ne p_stderr_read
    ) ()
