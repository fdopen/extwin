let try_finalize f x final y =
  let res =
    try
      f x
    with
    | exn ->
      let () = final y in
      raise exn
  in
  final y;
  res

let null_device = if Sys.win32 then "NUL" else "/dev/null"

#if OCAML_MAJOR < 4 || (OCAML_MAJOR = 4 && OCAML_MINOR < 3)
let char_lowercase_ascii c =
  if (c >= 'A' && c <= 'Z') then
    Char.unsafe_chr(code c + 32)
  else
    c

let char_uppercase_ascii c =
  if (c >= 'a' && c <= 'z') then
    Char.unsafe_chr(code c - 32)
  else
    c

let string_uppercase_ascii s = String.map char_uppercase_ascii s
let string_lowercase_ascii s = String.map char_lowercase_ascii s
#else
let char_lowercase_ascii = Char.lowercase_ascii
let char_uppercase_ascii = Char.uppercase_ascii
let string_lowercase_ascii = String.lowercase_ascii
let string_uppercase_ascii = String.uppercase_ascii
#endif

let regular_file_exists fln =
  let open Uv_fs_sync in
  match stat fln with
  | Error _ -> false
  | Ok x -> x.st_kind = S_REG

let uniq_list l =
  let rec add h acc = function
  | [] ->  List.rev acc
  | hd :: tl ->
    let nacc =
      if Hashtbl.mem h hd then
        acc
      else (
        Hashtbl.add h hd ();
        hd::acc
      )
    in
    add h nacc tl
  in
  let h = Hashtbl.create (List.length l) in
  add h [] l
