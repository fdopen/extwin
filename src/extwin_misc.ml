external putenv: string -> string option -> unit = "caml_extwin_putenv"
let unsetenv string = putenv string None

external is_wow64: unit -> bool = "caml_extwin_is_wow64"
let is_wow64 =
  if Sys.word_size = 64 || Sys.win32 = false then
    Lazy.from_val false
  else
    Lazy.from_fun is_wow64

let is_wow64 () = Lazy.force is_wow64
