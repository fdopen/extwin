#ifdef _WIN32
#undef _WIN32_WINNT
/* extended process and thread attribute necessary */
#define _WIN32_WINNT 0x0600
#endif

#define CAML_NAME_SPACE 1
#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/custom.h>
#include <caml/callback.h>
#include <caml/fail.h>
#include <caml/signals.h>
#include <caml/unixsupport.h>
#include <uwt-worker.h>

#ifdef _WIN32
#include <shellapi.h>
#include <errno.h>

static void
check_string_uerror(value str, char * cmdname)
{
  if ( strlen(String_val(str)) != caml_string_length(str) ){
    unix_error(ENOENT, cmdname, str);
  }
}

extern void caml_sys_error (value);
static void check_string_sys_error(value str)
{
  if ( strlen(String_val(str)) != caml_string_length(str) ){
    errno = ENOENT;
    caml_sys_error(str);
  }
}

static value
utf16_to_utf8_exn_unix(const wchar_t* utf16_buffer, char *cmdname, value ostr)
{
  value utf8_buffer;
  int utf8_len;
  utf8_len = WideCharToMultiByte(CP_UTF8,0,utf16_buffer,-1,NULL,0,NULL,NULL);
  if ( utf8_len == 0 ){
    win32_maperr(GetLastError());
    uerror(cmdname,ostr);
  }
  utf8_buffer = caml_alloc_string(utf8_len-1);
  utf8_len = WideCharToMultiByte(CP_UTF8,0,utf16_buffer,-1,
                                 String_val(utf8_buffer),utf8_len,NULL,NULL);
  if ( utf8_len == 0 ){
    win32_maperr(GetLastError());
    uerror(cmdname,ostr);
  }
  return utf8_buffer;
}

static value
utf16_to_utf8_uwt_result(const wchar_t* utf16_buffer)
{
  CAMLparam0();
  CAMLlocal1(utf8_buffer);
  int utf8_len;
  int ec;
  value ret;
  utf8_len = WideCharToMultiByte(CP_UTF8,0,utf16_buffer,-1,NULL,0,NULL,NULL);
  if ( utf8_len == 0 ){
    goto e_error;
  }
  utf8_buffer = caml_alloc_string(utf8_len-1);
  utf8_len = WideCharToMultiByte(CP_UTF8,0,utf16_buffer,-1,
                                 String_val(utf8_buffer),utf8_len,NULL,NULL);
  if ( utf8_len == 0 ){
  e_error:
    ec = uwt_translate_sys_error(GetLastError());
    if ( ec == UV_ENOMEM ){
      caml_raise_out_of_memory();
    }
    ret = caml_alloc_small(1,1);
    Field(ret,0) = Val_uwt_error(ec);
  }
  else {
    ret = caml_alloc_small(1,0);
    Field(ret,0) = utf8_buffer;
  }
  CAMLreturn(ret);
}

static wchar_t*
common_to_utf16(const char * buffer,UINT uCodePage)
{
  wchar_t * utf16_buffer;
  int utf16_len;
  if ( buffer == NULL ){
    return NULL;
  }
  utf16_len = MultiByteToWideChar(uCodePage,0,buffer,-1,NULL,0);
  if ( utf16_len == 0 ){
    return NULL;
  }
  utf16_buffer = malloc(sizeof(wchar_t) * utf16_len);
  if ( utf16_buffer == NULL ){
    return NULL;
  }
  utf16_len = MultiByteToWideChar(uCodePage,0,buffer,-1,utf16_buffer,
                                  utf16_len);
  if ( utf16_len == 0 ){
    free(utf16_buffer);
    return NULL;
  }
  return utf16_buffer;
}


static wchar_t*
utf8_to_utf16(const char* utf8_buffer){
  return(common_to_utf16(utf8_buffer,CP_UTF8));
}

static wchar_t*
sw_ansi_to_utf16(const char* buffer){
  return(common_to_utf16(buffer,CP_ACP));
}

static void handle_error(DWORD r, char *err_str, value err_param) Noreturn;
static void
handle_error(DWORD r, char *err_str, value err_param)
{
  if ( r == ERROR_NOT_ENOUGH_MEMORY ){
    caml_raise_out_of_memory();
  }
  win32_maperr(r);
  uerror(err_str,err_param);
  caml_failwith("uerror didn't throw"); /* uerror declared without Noreturn */
}

static wchar_t*
utf8_to_utf16_exn(const char* utf8_buffer, char *err_str, value err_param)
{
  wchar_t * x = utf8_to_utf16(utf8_buffer);
  if ( x == NULL ){
    DWORD r = GetLastError();
    handle_error(r,err_str,err_param);
  }
  return x;
}

CAMLprim value
caml_extwin_getcwd(value o_unit)
{
  enum {BUF_SIZE = 65536};
  wchar_t buf[BUF_SIZE];
  DWORD r = GetCurrentDirectoryW(BUF_SIZE - 1 , buf);
  (void) o_unit;
  if ( r == 0 || r >= BUF_SIZE - 1 ){
    win32_maperr(GetLastError());
    uerror("getcwd",Nothing);
  }
  buf[r] = L'\0';
  return (utf16_to_utf8_exn_unix(buf,"getcwd",Nothing));
}

CAMLprim value
caml_extwin_sys_getcwd(value o_unit)
{
  CAMLparam0();
  CAMLlocal2(tmp,ret);
  enum {BUF_SIZE = 65536};
  wchar_t buf[BUF_SIZE];
  DWORD r = GetCurrentDirectoryW(BUF_SIZE - 1,buf);
  (void) o_unit;
  if ( r == 0 || r >= BUF_SIZE - 1 ){
    int ec = uwt_translate_sys_error(GetLastError());
    if ( ec == UV_ENOMEM ){
      caml_raise_out_of_memory();
    }
    tmp = Val_uwt_error(ec);
    ret = caml_alloc_small(1,1);
    Field(ret,0) = tmp;
  }
  else {
    buf[r] = L'\0';
    ret = utf16_to_utf8_uwt_result(buf);
  }
  CAMLreturn(ret);
}

CAMLprim value
caml_extwin_chdir(value ostr)
{
  check_string_uerror(ostr,"chdir");
  wchar_t * ndir = utf8_to_utf16_exn(String_val(ostr),"chdir",ostr);
  DWORD r = SetCurrentDirectoryW(ndir);
  if ( r == 0 ){
    r = GetLastError();
    free(ndir);
    ndir = NULL;
    win32_maperr(r);
    uerror("chdir",Nothing);
  }
  free(ndir);
  return Val_unit;
}

CAMLprim value
caml_extwin_sys_chdir(value ostr)
{
  check_string_sys_error(ostr);
  int er = 0 ;
  wchar_t * ndir = uwt_utf8_to_utf16(String_val(ostr),&er);
  if ( ndir == NULL ){
    return (Val_uwt_int_result(er));
  }
  DWORD r = SetCurrentDirectoryW(ndir);
  if ( r == 0 ){
    r = GetLastError();
    free(ndir);
    er = uwt_translate_sys_error(r);
    return (Val_uwt_int_result(er));
  }
  else {
    free(ndir);
    return Val_unit;
  }
}

static value
copy_sys_argv(value o_argv)
{
  int osize = (int)Wosize_val(o_argv);
  if ( osize <= 0 ){
    return (Atom(0));
  }
  else {
    CAMLparam1(o_argv);
    CAMLlocal2(ret,tmp);
    int i;
    int r;
    char *p8;
    wchar_t * p16;
    ret = caml_alloc(osize,0);
    for ( i = 0 ; i < osize ; ++i ){
      p16 = sw_ansi_to_utf16(String_val(Field(o_argv,i)));
      if ( p16 == NULL ){
        ret = Atom(0);
        break;
      }
      p8 = uwt_utf16_to_utf8(p16,&r);
      free(p16);
      if ( p8 == NULL){
        ret = Atom(0);
        break;
      }
      tmp = caml_copy_string(p8);
      free(p8);
      Store_field(ret,i,tmp);
    }
    CAMLreturn(ret);
  }
}

static value
get_sys_argv(value o_argv, int is_byte)
{
  wchar_t **szArglist;
  int nArgs = 0;
  szArglist = CommandLineToArgvW(GetCommandLineW(), &nArgs);
  if( NULL == szArglist || nArgs == 0 ) {
    return (copy_sys_argv(o_argv));
  }
  else {
    CAMLparam1(o_argv);
    CAMLlocal2(ret,tmp);
    int start = 0;
    int i;
    int j;
    if ( is_byte ){
      /* first parameters could be from the call to ocamlrun */
      int osize = (int)Wosize_val(o_argv);
      if ( nArgs > osize ){
        start = nArgs - osize;
      }
    }
    ret = caml_alloc(nArgs-start,0);
    for ( i = start, j = 0 ;  i < nArgs ; i++, j++ ){
      int er;
      char * ctmp = uwt_utf16_to_utf8(szArglist[i],&er);
      if ( ctmp == NULL ){
        ret = copy_sys_argv(o_argv);
        break;
      }
      tmp = caml_copy_string(ctmp);
      free(ctmp);
      Store_field(ret,j,tmp);
    }
    CAMLreturn(ret);
  }
}

CAMLprim value
caml_extwin_sys_argv_native(value o_argv){
  return (get_sys_argv(o_argv,0));
}

CAMLprim value
caml_extwin_sys_argv_byte(value o_argv){
  return (get_sys_argv(o_argv,1));
}

CAMLprim value
caml_extwin_sys_exe_native(value o_str)
{
  (void) o_str;
  return Val_int(0);
}

CAMLprim value
caml_extwin_sys_exe_byte(value o_str)
{
  wchar_t * p16 = sw_ansi_to_utf16(String_val(o_str));
  if ( p16 == NULL ){
    return Val_int(1);
  }
  int r;
  char * p8 = uwt_utf16_to_utf8(p16,&r);
  free(p16);
  if ( p8 == NULL ){
    return Val_int(1);
  }
  else {
    CAMLparam0();
    CAMLlocal1(tmp);
    value ret;
    tmp = caml_copy_string(p8);
    free(p8);
    ret = caml_alloc_small(1,0);
    Field(ret,0) = tmp;
    CAMLreturn(ret);
  }
}

CAMLprim value
caml_extwin_environment_init(value o_unit)
{
  (void)o_unit;
  wchar_t * env = GetEnvironmentStringsW();
  if ( env == NULL ){
    win32_maperr(GetLastError());
    uerror("environment",Nothing);
  }
  value ret = caml_alloc_small(1,Abstract_tag);
  Field(ret,0) = (intnat)env;
  return ret;
}

CAMLprim value
caml_extwin_environment_clean(value o_env)
{
  wchar_t * env = (wchar_t*)Field(o_env,0);
  if ( env != NULL ){
    Field(o_env,0) = 0;
    FreeEnvironmentStringsW(env);
  }
  return Val_unit;
}

CAMLprim value
caml_extwin_environment(value o_env)
{
  CAMLparam1(o_env);
  CAMLlocal2(ar,tmp);
  wchar_t * env;
  wchar_t *p;
  size_t ar_size;

  env = (wchar_t*)Field(o_env,0);
  if ( env == NULL ){
    caml_invalid_argument("Unix.environment");
  }

  ar_size = 0;
  p = env;
  while (*p){
    while (*p){
      ++p;
    }
    ++p;
    ++ar_size;
  }

  if ( ar_size == 0 ) {
    ar = Atom(0);
  }
  else {
    size_t i = 0;
    ar = caml_alloc(ar_size, 0);
    p = env;
    while (*p){
      tmp=utf16_to_utf8_exn_unix(p,"environment",Nothing);
      Store_field(ar,i,tmp);
      do {
        ++p;
      } while (*p);
      ++p;
      ++i;
    }
  }
  Field(o_env,0) = 0;
  FreeEnvironmentStringsW(env);
  CAMLreturn(ar);
}

CAMLprim value
caml_extwin_getenv(value o_key)
{
  CAMLparam1(o_key);
  /*
    https://msdn.microsoft.com/de-de/library/windows/desktop/ms682653(v=vs.85).aspx
    The maximum size of a user-defined environment variable is 32,767 characters
  */
  enum {BUF_SIZE = 32768};
  wchar_t buf_value[BUF_SIZE];
  size_t len;
  value ret;
  check_string_uerror(o_key,"getenv");
  wchar_t * lookup = utf8_to_utf16_exn(String_val(o_key),"getenv",Nothing);
  len = GetEnvironmentVariableW(lookup, buf_value, BUF_SIZE - 1);
  if ( len == 0 || len >= BUF_SIZE - 1 ) {
    DWORD r = GetLastError();
    free(lookup);
    lookup = NULL;
    if ( r == ERROR_ENVVAR_NOT_FOUND ){
      caml_raise_not_found();
    }
    win32_maperr(r);
    uerror("getenv",o_key);
  }
  buf_value[BUF_SIZE-1] = 0;
  free(lookup);
  ret = utf16_to_utf8_exn_unix(buf_value,"getenv",o_key);
  CAMLreturn(ret);
}

CAMLprim value
caml_extwin_putenv(value o_key, value o_val)
{
  wchar_t * key = NULL;
  wchar_t * val = NULL;
  BOOL error_exit = TRUE;
  DWORD er = ERROR_SUCCESS;

  key = utf8_to_utf16(String_val(o_key));
  if ( key != NULL ){
    if ( o_val == Val_unit ){
      if ( SetEnvironmentVariableW(key,NULL) ){
        error_exit = FALSE;
      }
    }
    else {
      val = utf8_to_utf16(String_val(Field(o_val,0)));
      if (  val != NULL ){
        if ( SetEnvironmentVariableW(key,val) ){
          error_exit = FALSE;
        }
      }
    }
  }
  if ( error_exit ){
    er = GetLastError();
  }
  free(key);
  free(val);
  if ( error_exit ){
    handle_error(er,
                 o_val == Val_unit ? "unsetenv": "putenv",
                 o_key);
  }
  return Val_unit;
}

wchar_t *
search_exe_in_path(wchar_t * name)
{
  wchar_t * fullname, *filepart;
  size_t fullnamelen;
  DWORD retcode;

  fullnamelen = wcslen(name) + 1;
  if (fullnamelen < 256) fullnamelen = 256;
  while (1) {
    fullname = malloc(fullnamelen * sizeof(wchar_t));
    if ( fullname == NULL ){
      return NULL;
    }
    retcode = SearchPathW(NULL,              /* use system search path */
                          name,
                          L".exe",            /* add .exe extension if needed */
                          fullnamelen,
                          fullname,
                          &filepart);
    if (retcode == 0) {
      free(fullname);
      return _wcsdup(name);
    }
    if (retcode < fullnamelen)
      return fullname;
    free(fullname);
    fullnamelen = retcode + 1;
  }
}

static int
win_has_console(void)
{
  HANDLE h;
  h = CreateFile("CONOUT$", GENERIC_WRITE, FILE_SHARE_WRITE, NULL,
                 OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
  if (h == INVALID_HANDLE_VALUE) {
    return 0;
  } else {
    CloseHandle(h);
    return 1;
  }
}

static wchar_t *
convert_env ( value env )
{
  size_t ar_len = Wosize_val(env);
  if ( ar_len == 0 ){
    wchar_t *p = malloc(2*sizeof(wchar_t));
    if ( p == NULL) return NULL;
    p[0] = L'\0';
    p[1] = L'\0';
    return p;
  }
  wchar_t * ret = NULL;
  wchar_t *p;
  wchar_t ** c_ar = malloc(ar_len * sizeof(wchar_t*));
  if ( c_ar == NULL ) return NULL;
  size_t i;
  size_t whole_len = 0;
  int is_ok = 1;
  for ( i = 0 ; i < ar_len ; ++i ){
    c_ar[i] = utf8_to_utf16(String_val(Field(env,i)));
    if ( c_ar[i] == NULL ){
      ar_len = i;
      is_ok = 0;
      goto free_ret;
    }
    whole_len+= wcslen(c_ar[i]);
    whole_len++; /* zero */
  }
  whole_len++; /* end zero */
  ret = malloc(whole_len * sizeof(wchar_t));
  if ( ret == NULL ){
    is_ok = 0;
    goto free_ret;
  }
  p = ret;
  for ( i = 0 ; i < ar_len ; ++i ){
    wchar_t * q = c_ar[i];
    while ( *q != L'\0' ){
      *p = *q;
      ++p;
      ++q;
    }
    *p = L'\0';
    ++p;
  }
  *p = L'\0';

free_ret:
  for ( i = 0 ; i < ar_len ; ++i ){
    free(c_ar[i]);
  }
  free(c_ar);
  if ( is_ok == 0 ){
    free(ret);
    return NULL;
  }
  return ret;
}

typedef BOOL (WINAPI * InitializeProcThreadAttributeListFn)(
  LPPROC_THREAD_ATTRIBUTE_LIST lpAttributeList,
  DWORD dwAttributeCount,
  DWORD dwFlags,
  PSIZE_T lpSize
);

typedef BOOL (WINAPI * DeleteProcThreadAttributeListFn)(
  LPPROC_THREAD_ATTRIBUTE_LIST lpAttributeList
);

typedef BOOL (WINAPI * UpdateProcThreadAttributeFn)(
  LPPROC_THREAD_ATTRIBUTE_LIST lpAttributeList,
  DWORD dwFlags,
  DWORD_PTR Attribute,
  PVOID lpValue,
  SIZE_T cbSize,
  PVOID lpPreviousValue,
  PSIZE_T lpReturnSize
);

static InitializeProcThreadAttributeListFn InitializeProcThreadAttributeListPtr = NULL;
static DeleteProcThreadAttributeListFn DeleteProcThreadAttributeListPtr = NULL;
static UpdateProcThreadAttributeFn UpdateProcThreadAttributePtr = NULL;

CAMLprim value
caml_extwin_init(value o_unit)
{
  (void) o_unit;
  HMODULE kernel32 = GetModuleHandleW(L"kernel32.dll");
  if ( kernel32 ){
    InitializeProcThreadAttributeListPtr =
      (InitializeProcThreadAttributeListFn)
      (GetProcAddress(kernel32, "InitializeProcThreadAttributeList"));
    DeleteProcThreadAttributeListPtr =
      (DeleteProcThreadAttributeListFn)
      (GetProcAddress(kernel32, "DeleteProcThreadAttributeList"));
    UpdateProcThreadAttributePtr =
      (UpdateProcThreadAttributeFn)
      (GetProcAddress(kernel32, "UpdateProcThreadAttribute"));
  }
  return Val_unit;
}

static BOOL
dup_handle(HANDLE oldh, HANDLE *newh){
  HANDLE c_proc;
  c_proc = GetCurrentProcess(); /* pseudo handle, no close needed */
  if ( c_proc == NULL ||
       oldh == INVALID_HANDLE_VALUE ||
       oldh == NULL ||
       oldh == (HANDLE) -2) {
    /* -2 can be returned by _get_osfhandle */
    return FALSE;
  }
  if (!DuplicateHandle(c_proc,
                       oldh,
                       c_proc,
                       newh,
                       0,
                       TRUE,
                       DUPLICATE_SAME_ACCESS)) {
    return FALSE;
  }
  return TRUE;
}

CAMLprim value
caml_extwin_create_process_native(value cmd, value cmdline, value env,value cwd,
                                  value fd1, value fd2, value fd3,value ihandles)
{
  STARTUPINFOW si;
  PROCESS_INFORMATION pi;
  int flags;
  wchar_t * ucwd = NULL;
  wchar_t * ucmdline = NULL;
  wchar_t * ucmd = NULL;
  wchar_t * uexefile = NULL;
  wchar_t * uenv = NULL;
  DWORD er = ERROR_INVALID_DATA;
  BOOL inherit_handles = Long_val(ihandles) == 1;
  LPPROC_THREAD_ATTRIBUTE_LIST al = NULL;
  BOOL al_initialized = FALSE;
  HANDLE h_stdin_dup = INVALID_HANDLE_VALUE;
  HANDLE h_stdout_dup = INVALID_HANDLE_VALUE;
  HANDLE h_stderr_dup = INVALID_HANDLE_VALUE;
  BOOL create_process_success = FALSE;

  ucmdline = utf8_to_utf16_exn(String_val(cmdline),"create_process",cmd);
  ucmd = utf8_to_utf16(String_val(cmd));
  if ( ucmd == NULL ){
    goto error_e;
  }
  if ( Is_block(cwd) ){
    ucwd = utf8_to_utf16(String_val(Field(cwd,0)));
    if ( ucwd == NULL ){
      goto error_e;
    }
  }
  uexefile = search_exe_in_path(ucmd);
  if ( uexefile == NULL ){
    goto error_e;
  }

  if ( Is_block(env) ) {
    uenv = convert_env(Field(env, 0));
    if ( uenv == NULL ){
      goto error_e;
    }
  }
  ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
  ZeroMemory(&si, sizeof(STARTUPINFOW));
  si.cb = sizeof(STARTUPINFOW);
  si.dwFlags = STARTF_USESTDHANDLES ;
  si.hStdInput = Handle_val(fd1);
  si.hStdOutput = Handle_val(fd2);
  si.hStdError = Handle_val(fd3);
  flags = CREATE_UNICODE_ENVIRONMENT;
  si.dwFlags = STARTF_USESTDHANDLES;

  /* If we do not have a console window, then we must create one
     before running the process (keep it hidden for apparence).
     If we are starting a GUI application, the newly created
     console should not matter. */
  if ( ! win_has_console() ) {
    flags |= CREATE_NEW_CONSOLE;
    si.dwFlags |= STARTF_USESHOWWINDOW ;
    si.wShowWindow = SW_HIDE;
  }
  if ( inherit_handles == TRUE ||
       !InitializeProcThreadAttributeListPtr ||
       !DeleteProcThreadAttributeListPtr ||
       !UpdateProcThreadAttributePtr ){
    /* Prepare stdin/stdout/stderr redirection */
    /* Create the process */
    if ( CreateProcessW(uexefile, ucmdline, NULL, NULL,
                        TRUE, flags, uenv, ucwd, &si, &pi)) {
      create_process_success = TRUE;
    }
  }
  else {
    /* see: https://blogs.msdn.microsoft.com/oldnewthing/20111216-00/?p=8873 */
    STARTUPINFOEXW sie;
    SIZE_T al_mem_size = 0;
    HANDLE shared_handles[4];
    BOOL wres;

    flags |= EXTENDED_STARTUPINFO_PRESENT;

    /* always duplicate handels
       - they might be non-inheritable
       - dubious error can occur, if eg. both stderr and stdout are passed
         to CreateProcess. I couldn't find any documentation for it, but
         the problems don't occur, if we pass duplicates to CreateProcess
    */
    if ( !dup_handle(si.hStdInput,&h_stdin_dup) ||
         !dup_handle(si.hStdOutput,&h_stdout_dup) ||
         !dup_handle(si.hStdError,&h_stderr_dup) ){
      goto error_e;
    }

    shared_handles[0] = h_stdin_dup;
    shared_handles[1] = h_stdout_dup;
    shared_handles[2] = h_stderr_dup;
    shared_handles[3] = NULL;

    si.hStdInput = h_stdin_dup;
    si.hStdOutput = h_stdout_dup;
    si.hStdError = h_stderr_dup;

    /*
      First you call Initialize­Proc­Thread­Attribute­List with a NULL attribute
      list in order to determine how much memory you need to allocate for a
      one-entry attribute list.
    */
    wres = (*InitializeProcThreadAttributeListPtr)(NULL, 1, 0, &al_mem_size) ||
      GetLastError() == ERROR_INSUFFICIENT_BUFFER;

    if ( ! wres ){
      goto error_e;
    }

    /* After allocating the memory .. */
    al = malloc(al_mem_size);
    if ( al == NULL ){
      goto error_e;
    }

    /* ... you call Initialize­Proc­Thread­Attribute­List
       a second time to do the actual initialization.
    */
    if ( ! (*InitializeProcThreadAttributeListPtr)(al,1, 0, &al_mem_size) ){
      goto error_e;
    }
    al_initialized = TRUE;

    /*
      After creating the attribute list, you set the one entry by calling
      Update­Proc­Thread­Attribute­List.
    */
    if (! (*UpdateProcThreadAttributePtr)(al,
                                          0, PROC_THREAD_ATTRIBUTE_HANDLE_LIST,
                                          shared_handles,
                                          3 * sizeof(HANDLE),
                                          NULL, NULL) ){
      goto error_e;
    }

    ZeroMemory(&sie, sizeof(sie));
    sie.StartupInfo = si;
    sie.StartupInfo.cb = sizeof(sie);
    sie.lpAttributeList = al;

    if ( CreateProcessW(uexefile,
                        ucmdline,
                        NULL,
                        NULL,
                        TRUE,
                        flags,
                        uenv,
                        ucwd,
                        &sie.StartupInfo,
                        &pi) ){
      create_process_success = TRUE;
    }
  }
 error_e:
  if ( create_process_success != TRUE ){
    er = GetLastError();
  }
  if ( h_stderr_dup != INVALID_HANDLE_VALUE ){
    CloseHandle(h_stderr_dup);
  }
  if ( h_stdin_dup != INVALID_HANDLE_VALUE ){
    CloseHandle(h_stdin_dup);
  }
  if ( h_stdout_dup != INVALID_HANDLE_VALUE ){
    CloseHandle(h_stdout_dup);
  }
  if (al_initialized){
    (*DeleteProcThreadAttributeListPtr)(al);
  }
  free(al);
  free(uenv);
  free(ucmdline);
  free(ucmd);
  free(uexefile);
  if ( create_process_success != TRUE ){
    handle_error(er,"create_process",cmd);
  }
  else {
    CloseHandle(pi.hThread);
  }
  return Val_long(pi.hProcess);
}

CAMLprim value caml_extwin_create_process(value * argv, int argn)
{
  if ( argn != 8 ){
    caml_invalid_argument("create_process");
  }
  return caml_extwin_create_process_native(argv[0], argv[1], argv[2],
                                           argv[3], argv[4], argv[5],
                                           argv[6], argv[7]);
}

CAMLprim value
caml_extwin_fullpath(value o_path)
{
  CAMLparam1(o_path);
  CAMLlocal1(tmp);
  enum {BUF_SIZE = 32768};
  value ret = Val_unit;
  wchar_t realpath[BUF_SIZE];
  wchar_t * wpath = utf8_to_utf16(String_val(o_path));
  /* ansi version
    char * npath = _strdup(String_val(o_path));
    if ( npath == NULL ){
      caml_raise_out_of_memory();
    }
    caml_enter_blocking_section();
    char * p = _fullpath((char*)realpath,npath,_PATH_MAX);
    free(npath);
    caml_leave_blocking_section();
    } */
  if ( wpath != NULL ) {
    caml_enter_blocking_section();
    wchar_t * p = _wfullpath(realpath,wpath,BUF_SIZE-1);
    free(wpath);
    caml_leave_blocking_section();
    if ( p != NULL ){
      realpath[BUF_SIZE-1]=L'\0';
      tmp = utf16_to_utf8_exn_unix(realpath,"fullpath",o_path);
      ret = caml_alloc_small(1,0);
      Field(ret,0) = tmp;
    }
  }
  CAMLreturn(ret);
}

CAMLprim value
caml_extwin_is_executable(value o_path)
{
  CAMLparam1(o_path);
  wchar_t * path;
  value ret = Val_int(0);
  DWORD bin;
  mlsize_t path_len = caml_string_length(o_path);
  char * cstr = String_val(o_path);
  if ( path_len == 0 || path_len != strlen(cstr) ){
    goto endp;
  }
  path = utf8_to_utf16(cstr);
  /* ansi version
    char * npath = _strdup(String_val(o_path));
    if ( npath == NULL ){
      caml_raise_out_of_memory();
    }
    enter_blocking_section();
    if  ( GetBinaryType (npath, &bin) ){
      switch(bin){
      case SCS_32BIT_BINARY:
      case SCS_64BIT_BINARY:
      case SCS_DOS_BINARY:
      case SCS_WOW_BINARY:
        ret = Val_int(1);
      }
    }
    free(npath);
    leave_blocking_section();
  */
  if ( path == NULL ){
    goto endp;
  }
  caml_enter_blocking_section();
  if  ( GetBinaryTypeW (path, &bin) ){
    switch(bin){
    case SCS_32BIT_BINARY: /* fall */
    case SCS_64BIT_BINARY: /* fall */
    case SCS_DOS_BINARY: /* fall */
    case SCS_WOW_BINARY: /* fall */
      ret = Val_int(1);
    }
  }
  free(path);
  caml_leave_blocking_section();
endp:
  CAMLreturn(ret);
}

CAMLprim value
caml_extwin_wait(value ar_pids)
{
  CAMLparam1(ar_pids);
  CAMLlocal1(ret_ocaml);
  mlsize_t i;
  HANDLE* lpHandles;
  mlsize_t n_pids ;
  DWORD ecode;
  DWORD last_error = ERROR_SUCCESS;
  DWORD process_status;
  HANDLE return_handle;
  intnat lph_index;

  if ( ar_pids == Atom(0) ||
       !Is_block(ar_pids) ){
    caml_invalid_argument("caml_extwin_wait");
  }
  n_pids = Wosize_val(ar_pids);
  if ( n_pids == 0 || n_pids > MAXIMUM_WAIT_OBJECTS ){
    caml_invalid_argument("caml_extwin_wait");
  }
  lpHandles = caml_stat_alloc(n_pids * (sizeof *lpHandles));

  for ( i = 0; i < n_pids; i++ ){
    value x = Field(ar_pids,i);
    intnat p = Long_val(x);
    if ( p <= 0 ){
      caml_stat_free(lpHandles);
      caml_invalid_argument("caml_extwin_wait");
    }
    lpHandles[i] = (HANDLE) p;
  }

  caml_enter_blocking_section();
  ecode = WaitForMultipleObjects(n_pids, lpHandles, FALSE, INFINITE);
  if (ecode == WAIT_FAILED){
    last_error = GetLastError();
  }
  caml_leave_blocking_section();

  if ( last_error != ERROR_SUCCESS ){
    caml_stat_free(lpHandles);
    win32_maperr(last_error);
    uerror("caml_extwin_wait", Nothing);
  }
  lph_index = ecode - WAIT_OBJECT_0;
  if ( lph_index < 0 || (mlsize_t)lph_index >= n_pids ){
    caml_stat_free(lpHandles);
    unix_error(ERANGE, "caml_extwin_wait", Nothing);
  }
  return_handle = lpHandles[lph_index];
  caml_stat_free(lpHandles);
  if ( !GetExitCodeProcess(return_handle, &process_status) ){
    win32_maperr(GetLastError());
    uerror("caml_extwin_wait", Nothing);
  }

  if ( process_status == STILL_ACTIVE ){
    ret_ocaml= Val_int(0);
  }
  else {
    ret_ocaml = caml_alloc_small(2,0);
    Field(ret_ocaml,0) = Val_long(return_handle);
    Field(ret_ocaml,1) = Val_long(process_status);
  }
  CAMLreturn(ret_ocaml);
}

static HKEY
okey_to_hkey(value okey)
{
  switch(Long_val(okey)){
  case 0: return HKEY_LOCAL_MACHINE;
  case 1: return HKEY_CURRENT_CONFIG;
  case 2: return HKEY_CLASSES_ROOT;
  case 3: return HKEY_USERS;
  case 4: return HKEY_CURRENT_USER;
  default:
    caml_failwith("query_reg");
  }
}

/* TODO (both regquery functions):
   - proper distinction between error and no results
   - avoid exceptions
*/
CAMLprim value
caml_extwin_regquery_string(value okey, value osubkey, value ovalue)
{
#define QUERY_REG_BUFFER_SIZE 16385
  CAMLparam1(osubkey);
  CAMLlocal1(ret);
  HKEY hkey;
  wchar_t * subkey = NULL;
  wchar_t * cvalue = NULL;
  ret = Val_unit;

  wchar_t buf[QUERY_REG_BUFFER_SIZE] = {0};
  DWORD buf_size = sizeof(buf);

  hkey = okey_to_hkey(okey);
  subkey = utf8_to_utf16(String_val(osubkey));
  cvalue = utf8_to_utf16(String_val(ovalue));
  if ( subkey == NULL || cvalue == NULL ){
    goto endp;
  }

  if (RegGetValueW(hkey,
                   subkey,
                   cvalue,
                   REG_SZ | REG_EXPAND_SZ,
                   NULL,
                   (PVOID)&buf,
                   &buf_size) == ERROR_SUCCESS){
    value tmp;
    ret = utf16_to_utf8_exn_unix(buf,"req_query",osubkey);
    tmp = caml_alloc_small(1,0);
    Field(tmp,0) = ret;
    ret = tmp;
  }
endp:
  free(subkey);
  free(cvalue);
  CAMLreturn(ret);
#undef QUERY_REG_BUFFER_SIZE
}

CAMLprim value
caml_extwin_regquery_subkeys(value okey, value osubkey)
{
#define QUERY_REG_BUFFER_SIZE 16385
  CAMLparam1(osubkey);
  CAMLlocal2(ret,oar);
  HKEY hkey ;
  HKEY start_hkey;
  wchar_t * subkey;
  DWORD dret;
  DWORD i;

  wchar_t w_subkey[QUERY_REG_BUFFER_SIZE];
  DWORD n_sub_keys = 0;

  ret = Val_unit;
  start_hkey = okey_to_hkey(okey);

  subkey = utf8_to_utf16(String_val(osubkey));
  if ( subkey == NULL ){
    goto endp;
  }

  dret = RegOpenKeyExW(start_hkey,
                       subkey,
                       0,
                       KEY_READ,
                       &hkey);
  free(subkey);
  if ( dret != ERROR_SUCCESS ){
      goto endp;
  }

  dret = RegQueryInfoKeyW(hkey, NULL, NULL, NULL, &n_sub_keys, NULL, NULL, NULL,
                          NULL, NULL, NULL, NULL);
  if ( dret != ERROR_SUCCESS || n_sub_keys == 0 ){
    goto endp_clean;
  }
  oar = caml_alloc(n_sub_keys,0);
  for ( i = 0 ; i < n_sub_keys ; ++i ) {
    DWORD max_sub_key_length;
    value tmp;
    max_sub_key_length = QUERY_REG_BUFFER_SIZE;
    dret = RegEnumKeyExW(hkey,
                         i,
                         w_subkey,
                         &max_sub_key_length,
                         NULL,
                         NULL,
                         NULL,
                         NULL);
    if ( dret != ERROR_SUCCESS ){
      goto endp_clean;
    }
    tmp = utf16_to_utf8_exn_unix(w_subkey,"req_query_subkeys",osubkey);
    Store_field(oar, i, tmp);
  }
  ret = caml_alloc_small(1,0);
  Field(ret,0) = oar;

endp_clean:
  RegCloseKey(hkey);
endp:
  CAMLreturn(ret);
#undef QUERY_REG_BUFFER_SIZE
}

typedef BOOL (WINAPI *wow64_help) (HANDLE, PBOOL);

CAMLprim value
caml_extwin_is_wow64(value o_unit)
{
  BOOL is_wow64 = FALSE;
  wow64_help fx;
  (void) o_unit;
  fx = (wow64_help) GetProcAddress(GetModuleHandleW(L"kernel32"),
                                   "IsWow64Process");
  if(NULL != fx){
    if (!fx(GetCurrentProcess(),&is_wow64)) {
      win32_maperr(GetLastError());
      uerror("is_wow64", Nothing);
    }
  }
  return ( Val_long(is_wow64 == TRUE) );
}

#else

#define UNAVAILABLE_PRIM1(primname)                               \
  CAMLprim value primname(value o1)  Noreturn;                    \
  CAMLprim value primname(value o1)                               \
  {                                                               \
    (void) o1;                                                    \
    caml_failwith("extwin primitive not available: " #primname);  \
  }

#define UNAVAILABLE_PRIM2(primname)                               \
  CAMLprim value primname(value o1,value o2)  Noreturn;           \
  CAMLprim value primname(value o1,value o2)                      \
  {                                                               \
    (void) o1; (void) o2;                                         \
    caml_failwith("extwin primitive not available: " #primname);  \
  }

#define UNAVAILABLE_PRIM3(primname)                               \
  CAMLprim value primname(value o1,value o2, value o3)  Noreturn; \
  CAMLprim value primname(value o1,value o2, value o3)            \
  {                                                               \
    (void) o1; (void) o2; (void) o3;                              \
    caml_failwith("extwin primitive not available: " #primname);  \
  }

#define UNAVAILABLE_PRIM6(primname)                                     \
  CAMLprim value primname(value o1,value o2,value o3, value o4, value o5, value o6) Noreturn; \
  CAMLprim value primname(value o1,value o2,value o3, value o4, value o5, value o6) \
  {                                                                     \
    (void) o1; (void) o2; (void) o3; (void) o4; (void) o5; (void) o6;   \
    caml_failwith("extwin primitive not available: " #primname);        \
  }

UNAVAILABLE_PRIM1(caml_extwin_sys_argv_native)
UNAVAILABLE_PRIM1(caml_extwin_sys_argv_byte)
UNAVAILABLE_PRIM1(caml_extwin_sys_exe_byte)
UNAVAILABLE_PRIM1(caml_extwin_sys_exe_native)
UNAVAILABLE_PRIM1(caml_extwin_sys_chdir)
UNAVAILABLE_PRIM1(caml_extwin_sys_getcwd)
UNAVAILABLE_PRIM1(caml_extwin_environment)
UNAVAILABLE_PRIM1(caml_extwin_environment_init)
UNAVAILABLE_PRIM1(caml_extwin_environment_clean)
UNAVAILABLE_PRIM1(caml_extwin_getenv)
UNAVAILABLE_PRIM1(caml_extwin_getcwd)
UNAVAILABLE_PRIM1(caml_extwin_chdir)
UNAVAILABLE_PRIM1(caml_extwin_fullpath)
UNAVAILABLE_PRIM1(caml_extwin_is_executable)
UNAVAILABLE_PRIM1(caml_extwin_wait)
UNAVAILABLE_PRIM2(caml_extwin_putenv)
UNAVAILABLE_PRIM2(caml_extwin_create_process)
UNAVAILABLE_PRIM6(caml_extwin_create_process_native)
UNAVAILABLE_PRIM3(caml_extwin_regquery_string)
UNAVAILABLE_PRIM2(caml_extwin_regquery_subkeys)
UNAVAILABLE_PRIM1(caml_extwin_is_wow64)

CAMLprim value
caml_extwin_init(value o_unit)
{
  (void) o_unit;
  return Val_unit;
}
#endif
