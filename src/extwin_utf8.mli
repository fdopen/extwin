(**
   I/O functions that accepts utf8 string as parameters
   and return utf8 encoded strings

   Known differences to the standard modules:

   - Unix.O_APPEND does work, but you can't pass such an handles to
     create_process :)

   - files are always opened with
     (FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE) in order to mimic
     unix semantics. You can't disable this.

   - The strings inside Sys_error _ exceptions will differ and the error codes
     inside Unix.Unix_error might also differ slightly

   - in byte code, Sys.argv and Sys.executable_name might be unreliable.
*)

module Pervasives : sig
  val open_out_gen : open_flag list -> int -> string -> out_channel
  val open_out : string -> out_channel
  val open_out_bin : string -> out_channel
  val open_in_gen : open_flag list -> int -> string -> in_channel
  val open_in : string -> in_channel
  val open_in_bin : string -> in_channel
end

module Sys : sig
  (** argv and executable_name will only work in native code *)
  val argv : string array
  val executable_name : string

  val getenv : string -> string
  val file_exists : string -> bool
  val is_directory : string -> bool
  val remove : string -> unit
  val rename : string -> string -> unit
  val chdir : string -> unit
  val getcwd : unit -> string
  val readdir : string -> string array
end

module Filename : sig
  val temp_dir_name : string
  val set_temp_dir_name : string -> unit
  val get_temp_dir_name : unit -> string
  val temp_file : ?temp_dir:string -> string -> string -> string
#if OCAML_MAJOR < 4 || (OCAML_MAJOR = 4 && OCAML_MINOR < 3)
  val open_temp_file :
    ?mode:open_flag list ->
    ?temp_dir:string -> string -> string -> string * out_channel
#else
  val open_temp_file :
    ?mode:open_flag list -> ?perms:int ->
    ?temp_dir:string -> string -> string -> string * out_channel
#endif
end

module Unix : sig
  val openfile : string -> Unix.open_flag list -> int -> Unix.file_descr
  val unlink : string -> unit
  val mkdir : string -> int -> unit
  val rmdir : string -> unit
  val stat : string -> Unix.stats
  val lstat : string -> Unix.stats
  val rename : string -> string -> unit
  val link : string -> string -> unit
#if OCAML_MAJOR < 4 || (OCAML_MAJOR = 4 && OCAML_MINOR < 3)
  val symlink : string -> string -> unit
#else
  val symlink : ?to_dir:bool -> string -> string -> unit
#endif
  val readlink : string -> string
  val utimes : string -> float -> float -> unit
  val access : string -> Unix.access_permission list -> unit
  val chmod : string -> int -> unit
  val fchmod : Unix.file_descr -> int -> unit
  val environment : unit -> string array
  val getenv : string -> string
  val putenv : string -> string -> unit
  val create_process :
    string ->
    string array ->
    Unix.file_descr -> Unix.file_descr -> Unix.file_descr -> int
  val create_process_env :
    string ->
    string array ->
    string array ->
    Unix.file_descr -> Unix.file_descr -> Unix.file_descr -> int
  val getcwd : unit -> string
  val chdir : string -> unit
  val getlogin : unit -> string
end
