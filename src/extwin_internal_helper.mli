val try_finalize : ('a -> 'b) -> 'a -> ('c -> unit) -> 'c -> 'b
val null_device : string
val char_lowercase_ascii : char -> char
val char_uppercase_ascii : char -> char
val string_lowercase_ascii : string -> string
val string_uppercase_ascii : string -> string
val regular_file_exists : string -> bool
val uniq_list : 'a list -> 'a list
