external caml_extwin_init: unit -> unit = "caml_extwin_init" "noalloc"
let () = caml_extwin_init ()

external win_create_process :
  cmd:string ->
  cmdline:string ->
  env:string array option ->
  cwd:string option ->
  stdin:Unix.file_descr ->
  stdout:Unix.file_descr ->
  stderr:Unix.file_descr ->
  inherits:bool ->
  int = "caml_extwin_create_process" "caml_extwin_create_process_native"

let make_cmdline args =
  let maybe_quote f =
    if String.contains f ' ' || String.contains f '\t' ||
       String.contains f '\"' || f = ""
    then Filename.quote f
    else f
  in
  String.concat " " (List.map maybe_quote (Array.to_list args))

let is_atty x =
  match Uwt_base.Conv.file_of_file_descr x with
  | None -> false
  | Some x -> Uwt_base.Misc.guess_handle x = Uwt_base.Misc.Tty

let create_process_full
    ?env ?cwd ?(inherits=false) ~cmd ~args ~stdin ~stdout ~stderr () =
  let env = match env with
  | None -> None
  | Some ar ->
    Some (
      Array.to_list ar |>
      List.filter ((<>) "") |>
      Array.of_list )
  and cmdline = make_cmdline args
  and inherits = match inherits with
  | true -> true
  | false -> is_atty stdin || is_atty stdout || is_atty stderr
  in
  win_create_process
    ~cmd
    ~cmdline
    ~env
    ~cwd
    ~stdin
    ~stdout
    ~stderr
    ~inherits

let create_process cmd args stdin stdout stderr =
  create_process_full ~cmd ~args ~stdin ~stdout ~stderr ~inherits:true ()

let create_process_env cmd args env stdin stdout stderr =
  create_process_full ~env ~cmd ~args ~stdin ~stdout ~stderr ~inherits:true ()
