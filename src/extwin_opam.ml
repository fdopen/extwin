open Extwin_dirty
module Eih = Extwin_internal_helper

module Win32_only = struct
  external is_executable : string -> bool = "caml_extwin_is_executable"

  type wait_one =
    | None_found
    | Finished of int * int

  external wait_ar: int array -> wait_one = "caml_extwin_wait"
  let rec wait ar =
    match wait_ar ar with
    | None_found -> wait ar
    | Finished(a,b) -> a, Unix.WEXITED b

  let wait ar =
    (* WaitForMultipleObjects require uniqueness *)
    Array.to_list ar |> Eih.uniq_list |> Array.of_list |> wait

  type sresult =
    | Nothing_found
    | Maybe_script
    | Exe of string

  let path_sep_rex = Re_pcre.regexp ";"
  let search_in_path ?paths = function
  | "" -> Nothing_found
  | cmd ->
    let paths =
      match paths with
      | Some x -> x
      | None ->
        try
          Sys.getenv "PATH" |> Re_pcre.split ~rex:path_sep_rex
        with
        | Not_found -> [] (* the shell will decide what to do *)
    in
    let is_exe = Filename.check_suffix (Eih.string_lowercase_ascii cmd) ".exe" in
    let rec iter = function
    | [] -> Nothing_found
    | ""::tl -> iter tl
    | dir::tl ->
      let fln = Filename.concat dir cmd in
      if Eih.regular_file_exists fln then
        if is_executable fln then
          Exe fln
        else
          Maybe_script
      else if is_exe then
        iter tl
      else
        let fln_exe = fln ^ ".exe" in
        if Eih.regular_file_exists fln_exe then
          if is_executable fln_exe then
            Exe fln_exe
          else
            Maybe_script
        else
          iter tl
    in
    iter paths

  let create_process_full = Extwin_utf8_proc.create_process_full
end

module NUnix = struct
  (* generic quote and unixquote are taken from ocaml source *)
  let generic_quote quotequote s =
    let l = String.length s in
    let b = Buffer.create (l + 20) in
    Buffer.add_char b '\'';
    for i = 0 to l - 1 do
      if s.[i] = '\'' then
        Buffer.add_string b quotequote
      else
        Buffer.add_char b  s.[i]
    done;
    Buffer.add_char b '\'';
    Buffer.contents b

  let quote =
    if Sys.win32 then
      generic_quote "'\\''"
    else
      Filename.quote
end

let replace ~old ~new' s =
  let len = String.length s in
  let b = Bytes.create len in
  for i = 0 to pred len do
    let x = s.[i] in
    Bytes.set b i (if x = old then new' else x)
  done;
  if len > 1 then (
    let c1 = Bytes.get b 0 in
    let c2 = Bytes.get b 1 in
    if c2 = ':' && c1 >= 'a' && c1 <= 'z' &&
       ( len = 2 || Bytes.get b 2 = new') then
      Bytes.set b 0 (Eih.char_uppercase_ascii c1)
  );
  Bytes.unsafe_to_string b

let slashify s =
  if Sys.win32 then
    replace ~old:'\\' ~new':'/' s
  else
    s

let unslashify s =
  if Sys.win32 then
    replace ~old:'/' ~new':'\\' s
  else
    s

module Cygpath = struct

  type winpath =
    (* C:/a/b/c -> Win32_drive(C,["c","b","a"]) *)
    | Win32_drive of char * string list
    (* //h/a/b/c -> Win32_remove(h,["c","b","a"]) *)
    | Win32_remote of string * string list

  module Mmap = Map.Make(struct type t = winpath let compare = compare end)

  (* cached entries from the mount table. We want to call mount.exe only once *)
  let mount_entries = ref None

  let drive_re = Re_pcre.regexp "[a-zA-Z]:"
  let unc_re = Re_pcre.regexp "//([^/]+)(/.*)"
  let slash_re = Re_pcre.regexp "/"
  let space_rex = Re_pcre.regexp "\\s+"

  let to_winpath filename =
    if Re_pcre.pmatch ~rex:drive_re filename then
      let drive = Eih.char_lowercase_ascii filename.[0]
      and len = String.length filename in
      if String.length filename < 4 then
        Some(Win32_drive(drive,[]))
      else
      let s = String.sub filename 3 (len - 3) in
      let l = Re_pcre.split ~rex:slash_re s |> List.rev in
      Some(Win32_drive(drive,l))
    else
    try
      match Re_pcre.extract ~rex:unc_re filename with
      | [| _ ; host ; path |] ->
        let l = Re_pcre.split ~rex:slash_re path in
        Some(Win32_remote(host, l))
      | _ -> None
    with
    | Not_found -> None

  let re_lc_all = Re_pcre.regexp "^LC_ALL="
  let re_lc_ctype = Re_pcre.regexp "^LC_CTYPE="
  let re_lang = Re_pcre.regexp "^LANG="
  let newline_rex = Re_pcre.regexp "\n+"
  let get_mount_lines log =
    let buf = Buffer.create 8192
    and ebuf = Buffer.create 10 in
    let stdin = `Null
    and stderr = `Buffer ebuf
    and stdout = `Buffer buf in
    let f el ac =
      if Re_pcre.pmatch ~rex:re_lc_all el ||
         Re_pcre.pmatch ~rex:re_lc_ctype el ||
         Re_pcre.pmatch ~rex:re_lang el then
        ac
      else
        el::ac
    in
    try
      let env =
        let t = Array.fold_right f (Unix.environment ()) [] in
        let t = "LC_ALL=C.UTF-8"::"LC_CTYPE=C.UTF-8"::"LANG=C.UTF-8"::t in
        Array.of_list t
      in
      let r = Extwin_run.run ~stdout ~stderr ~stdin ~env "mount.exe" [] in
      if r <> 0 then (
        Printf.sprintf "call to mount.exe failed:%s" (Buffer.contents ebuf)
        |> log;
        []
      )
      else
        Buffer.contents buf |> Re_pcre.split ~rex:newline_rex
    with
    | Unix.Unix_error(e,s,_) ->
      Printf.sprintf
        "call to mount.exe failed: %s (%s)"
        (Unix.error_message e)
        s
      |> log ;
      []

  let get_mount_htls log =
    match !mount_entries with
    | Some x -> x
    | None ->
      (* Run "mount" and save the result *)
      let lines = get_mount_lines log in
      let add_mount_entry accu line =
        match Re_pcre.split ~rex:space_rex line with
        | [ w_path; "on"; p_path; "type"; _type; _flags ] ->
          begin match to_winpath w_path with
          | None ->
            Printf.sprintf "invalid line in mount table: %s" line |> log;
            accu
          | Some w ->
            Mmap.add w p_path accu
          end
        | _ ->
          Printf.sprintf "invalid line in mount table: %s" line |> log;
          accu
      in
      let r = List.fold_left add_mount_entry Mmap.empty lines in
      mount_entries := Some r;
      r

  let rec walk_path mmap s accu =
    try
      let posix = Mmap.find s mmap in
      if posix = "/" then
        "/" ^ (String.concat "/" accu)
      else
        String.concat "/" (posix::accu)
    with
    | Not_found ->
      match s with
      | Win32_drive(drive,[]) ->
        let prefix = "/cygdrive/" ^ (String.make 1 drive) in
        String.concat "/" (prefix::accu)
      | Win32_remote(host,[]) ->
        "//" ^ host ^ "/" ^ (String.concat "/" accu)
      | Win32_drive(root,hd::tl) ->
        walk_path mmap (Win32_drive(root,tl)) (hd::accu)
      | Win32_remote(root,hd::tl) ->
        walk_path mmap (Win32_remote(root,tl)) (hd::accu)

  (* Don't forget to update, currently the string is considered
     to be utf8 encoded *)
  external fullpath : string -> string option = "caml_extwin_fullpath"

  let cygpath log (orig_s:string) : string =
    let slash_orig_s = slashify orig_s in
    match fullpath orig_s with
    | exception Unix.Unix_error(e,_,_) ->
      Printf.sprintf
        "call to _wfullpath failed: %s"
        (Unix.error_message e)
      |> log ;
      slash_orig_s
    | None ->
      Printf.sprintf "call to _wfullpath failed for %s" orig_s |> log;
      slash_orig_s
    | Some s ->
      let s = slashify s in
      match to_winpath s with
      | None -> slash_orig_s
      | Some splitted ->
        let mmap = get_mount_htls log in
        let s = walk_path mmap splitted [] in
        let orig_len = String.length slash_orig_s in
        (* keep trailing slashes, like cygpath *)
        if orig_len > 0 && slash_orig_s.[orig_len - 1] = '/' &&
           s.[String.length s - 1] <> '/'
        then
          if orig_len < 2 || orig_len <> 3 || slash_orig_s.[1] <> ':' then
            s ^ "/"
          else
            (* Special case for X:\\ which will be translated to
               /cygdrive/x not, /cygdrive/x/
            *)
            let c = slash_orig_s.[0] in
            if (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') then
              s
            else
              s ^ "/"
        else
          s
end

let cygpath =
  if Sys.win32 = false then
    fun _ s -> s
  else
    fun l s ->
      if Filename.is_relative s then
        slashify s
      else
        Cygpath.cygpath l s

let quote_needed s =
  let len = String.length s in
  if len = 0 then
    true
  else
    let rec iter i =
      if i < 0 then
        false
      else
        match s.[i] with
        | 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '-' | '+' | '.' | '/' ->
          iter (pred i)
        | _ -> true
    in
    iter (len - 1)
(*
let cygwin_bin_dir =
  lazy (
    let open Win32_only in
    match search_in_path "mount.exe" with
    | Nothing_found
    | Maybe_script -> None
    | Exe s ->
      match Cygpath.fullpath s with
      | None -> None
      | Some x -> Some (Filename.dirname x ))

let is_cygwin_exec fln =
  match Lazy.force cygwin_bin_dir with
  | None -> false
  | Some cygwin_bin_dir ->
    let fln =
      if Filename.is_implicit fln &&
         String.contains fln '\\' = false &&
         String.contains fln '/' = false then
        let open Win32_only in
        match search_in_path fln with
        | Nothing_found
        | Maybe_script -> fln
        | Exe s -> s
      else
        fln
    in
    match Cygpath.fullpath fln with
    | None -> false
    | Some x -> Filename.dirname x = cygwin_bin_dir *)

let do_need_shell args =
  let rec check_cmd s i =
    if i < 0 then
      false
    else
      match s.[i] with
      | '{' | '}' | '*' | '?' | '[' | ']' -> true
      | _ -> check_cmd s (pred i) in
  let check_cmd s = check_cmd s (String.length s - 1) in
  List.exists check_cmd args

let cygwin_shell_rewrite ~shell_cmd ~shell_args ~force_shell (cmd:string) args =
  let make_dash_cmd cmd args =
    let buf = Buffer.create 256 in
    let add s =
      (* Yes, this will lead intentionally to a leading whitespace!
         It seems to be necessary, in order to avoid globbing.
         Otherwise cygwin will mess up quotes. *)
      Buffer.add_char buf ' ';
      if quote_needed s then
        NUnix.quote s |> Buffer.add_string buf
      else
        Buffer.add_string buf s
    in
    add (slashify cmd);
    List.iter add args;
    shell_cmd, shell_args @ [Buffer.contents buf]
  in
  if Sys.win32 = false || cmd = shell_cmd then
    cmd,args
  else if force_shell || do_need_shell args then
    make_dash_cmd cmd args
  else if
    Filename.is_implicit cmd = false ||
    String.contains cmd '\\' ||
    String.contains cmd '/'
  then
    if Eih.regular_file_exists cmd then
      if Win32_only.is_executable cmd then
        unslashify cmd,args
      else
        make_dash_cmd cmd args
    else if Filename.check_suffix (Eih.string_lowercase_ascii cmd) ".exe" then
      (* we always give dash a chance to handle the issue for us, if we can't
         find an executable *)
      make_dash_cmd cmd args
    else
      let fln = cmd ^ ".exe" in
      if Win32_only.is_executable fln then (* exe *)
        unslashify fln,args
      else
        make_dash_cmd cmd args
  else
    match Win32_only.search_in_path cmd with
    | Win32_only.Maybe_script
    | Win32_only.Nothing_found -> make_dash_cmd cmd args
    | Win32_only.Exe fln -> unslashify fln,args

module Unix = NUnix
